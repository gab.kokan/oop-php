<?php
  require('animal.php');
  require('frog.php');
  require('ape.php');

  $sheep = new Animal('Shaun', 2, 'false');
  echo "<br> <b>RELEAS 0</b><br>";
  echo $sheep->name . "<br>";
  echo $sheep->legs . "<br>";
  echo $sheep->cold_blooded . "<br>";

  echo "<br> <b>RELEAS 1</b><br>";
  $sungokong = new Ape('Kera Sakti',2 ,'Auoooo' );
  echo $sungokong->name . "<br>";
  echo $sungokong->yell . "<br>";
  echo $sungokong->cold_blooded . "<br><br>";

  $kodok = new Frog('Buduk', 4, 'Hop Hop');
  echo $kodok->name . "<br>";
  echo $kodok->jump . "<br>";
  echo $kodok->cold_blooded . "<br>";



?>
