<?php

  class Frog
  {
    public $name;
    public $jump;
    public $cold_blooded;

    function __construct($name,$jump,$cold_blooded)
    {
      $this->name = $name;
      $this->jump = $jump;
      $this->cold_blooded = $cold_blooded;
    }
  }

?>
