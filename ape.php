<?php

  class Ape
  {
    public $name;
    public $yell;
    public $cold_blooded;

    function __construct($name,$yell,$cold_blooded)
    {
      $this->name = $name;
      $this->yell = $yell;
      $this->cold_blooded = $cold_blooded;
    }
  }

?>
